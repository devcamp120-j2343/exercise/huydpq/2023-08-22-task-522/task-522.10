import { Person2Outlined, Visibility, VisibilityOff } from "@mui/icons-material"
import { Box, FormControl, IconButton, Input, InputAdornment, InputLabel, Typography, Button } from "@mui/material"
import React from "react";

const LoginContent = () => {
    const [showPassword, setShowPassword] = React.useState(false);

    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };
    return (
        <>
            <Box className="loginBox">
                <Typography variant="h4" className="loginTitle">Login</Typography>

                <FormControl sx={{ mt: '35px', width: '100%', borderBottom: "1px solid #fff" }} variant="standard">
                    <InputLabel htmlFor="username" className="loginItem">
                        Username
                    </InputLabel>
                    <Input
                        className="loginInput"
                        id="username"
                        endAdornment={
                            <InputAdornment position="end">
                                <Person2Outlined sx={{ color: "#FFF !important" }} />
                            </InputAdornment>
                        }
                    />
                </FormControl>

                <FormControl sx={{ mt: '40px', width: '100%',borderBottom: "1px solid #fff" }} variant="standard">
                    <InputLabel htmlFor="password" className="loginItem">Password</InputLabel>
                    <Input
                        id="password"
                        className="loginInput"
                        type={showPassword ? 'text' : 'password'}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    sx={{ color: "#FFF !important" }}
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                >
                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        }
                    />
                </FormControl>
                <Button sx={{ mt: '40px', width: '100%' }} className="loginButton" variant="contained">Login</Button>

            </Box>
        </>
    )
}

export default LoginContent