import { Container, Grid } from "@mui/material"
import LoginContent from "../components/loginContent"

const Login = () => {
    return (
        <>
            <Container maxWidth="xxl" className="bgLogin">
                <Grid container>
                    <Grid item sm={4} md={4} lg={5} xl={4} className="loginContainer">
                        <LoginContent/>
                    </Grid>
                </Grid>
            </Container>

        </>
    )
}

export default Login